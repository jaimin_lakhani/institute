/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package institute;

/**
 *
 * @author jaiminlakhani
 */
public class Institute {

    /**
     * @param args the command line arguments
     */
    String instituteName; 
    private List<Department> departments;
    
    Institute(String instituteName, List<Department> departments) {
        this.instituteName = instituteName;
        this.departments = departments;
    }
    
    punlic int getTotalStudentsInstitute() {
        int noOfStudents = 0;
        List<Student> students;
        for(Department dept : departments) {
            students = dept.getStudents();
            for(Student : students) {
                noOfStudents++;
            }
        }
        return noOfStudents;
    }
    
}
